#include "mbed.h"
#include "app.h"
#include "iTest.hpp"

extern SDBlockDevice sd;
USBMSD usb(&sd);

int App::init(void)
{    
    int success = 1;

    ThisThread::sleep_for(500);
    
    this->mode = this->modePin.read();
    
    if(this->mode == APP_MSD_MODE)
    {
        printf("Started in mode < APP_MSD_MODE > \n\r");
        this->ledToggle.attach(callback(this, &App::toggleLeds), 0.1);
    }
    else if(this->mode == APP_BRIDGE_MODE)
    {
        float v_ref = m_audio1.get_reference_voltage();
        printf("Ref Voltage: %d", (int)v_ref);
        if (fileInst.init() != 0)
        {
            // set error led
            this->led1.write(1);
            while(1);
        }
        else 
        {
            fileInst.start();
            f.init(500);
            f.start();
            
        }

    }
    
    return success;
}

void App::process(void)
{
    while(true)
    {
        if (this->mode == APP_MSD_MODE)
        {
            usb.process();
        }
        else 
        {
#if ENABLE_MANUAL_TEST
            manualTestWrapper();
#endif
#if ENABLE_DATASET_TEST
            datasetTestWrapper();
#endif
            ThisThread::sleep_for(1);
        } 
           
    }
}

void App::toggleLeds(void)
{
    this->led1 = !this->led1;
}

void App::manualTestWrapper(void)
{
    if(this->modePin.read() == 1)
    {
        m_mtest.start(); 
        ThisThread::sleep_for(1000); 
        printf("Test start detected! \n\r");                
    }
}

void App::datasetTestWrapper(void)
{
    
}
