#ifndef FLOWMETER_H
#define FLOWMETER_H
#include "mbed.h"
#include "counter.hpp"


typedef enum
{
    FLOWMETER_MODEL,
    FLOWMETER_MODEL_MAX
}eFloweterModel;

class flowmeter : public  Counter
{


public:
    flowmeter(PinName pin, eFloweterModel model): Counter(pin), thread_(osPriorityNormal)
    {

    }
    ~flowmeter(){}
    /*!
        Initialize flowmeter:
            period_ms: Range values 50mS -> 500mS
     */
    void init(uint16_t period_ms);
    void start(void);
    void stop(void);
    float getInstantFlow(void);
    float getTotalFlow(void);
    void resetCount(void);
    void run(void);
private:
    void updateInstantFlow(void);
    Thread thread_;
    uint16_t m_period;
    uint64_t m_instantFlowLastCount;
    volatile float m_instantFlow;
};


#endif