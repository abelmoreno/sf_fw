#include "flowmeter.hpp"


void flowmeter::init(uint16_t period_ms)
{
    m_period = period_ms;
}
void flowmeter::start(void)
{
    thread_.start(callback(this, &flowmeter::run));
}
void stop(void)
{

}
float flowmeter::getInstantFlow(void)
{
    return m_instantFlow; 
}
float flowmeter::getTotalFlow(void)
{
    float totalFlow;
    /*! Based on formula: Freq = 23 * Q(L/min) */
    // 23(Hz) pulses / second to achieve 1 Liter
    // 1 Liter -> 23 * 60 pulses
    
    totalFlow = this->read() / (23.0 * 60.0);
    //printf("%d \n\r", (int)this->read()); 
    return totalFlow; 
}
void flowmeter::resetCount(void)
{

}
void flowmeter::run(void)
{
    double decimal;
    while(true)
    {
        updateInstantFlow();
        double fract = modf((double)this->getTotalFlow(), &decimal);
        //printf("totalFlow: %d.%.2d L \n\r",(int)decimal,(int)(100.0*fract) );
        fract = modf((double)this->getInstantFlow(), &decimal);
        //printf("instantFlow : %d.%.2d L/min \n\r", (int)decimal,(int)(100.0*fract));
        ThisThread::sleep_for(m_period);
    }

}

void flowmeter::updateInstantFlow(void)
{
    uint64_t countIncrement, actualCount;
    float frequency;
    actualCount = this->read();

    countIncrement = actualCount - m_instantFlowLastCount;
    m_instantFlowLastCount = actualCount;

    frequency = (float)countIncrement * (float)(1000.0 / this->m_period);
    this->m_instantFlow = frequency / 23.0;    
}