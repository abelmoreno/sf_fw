
#ifndef APP_H
#define APP_H

#include "mbed.h"
#include "FileManager/filemanager.hpp"
#include "flowmeter.hpp"

#include "manual.hpp"

#define APP_MSD_MODE    1
#define APP_BRIDGE_MODE 0

#define APP_MODEL_PIN    USER_BUTTON
#define APP_LED1_PIN     PA_5



class App 
{       
  public:             
  App(void):
    fileInst(),
    modePin(APP_MODEL_PIN),
    led1(APP_LED1_PIN),
    f(PB_9, FLOWMETER_MODEL),
    m_audio1(A0),
    m_audio2(A1),
    m_mtest(fileInst,m_audio1,m_audio2)
    {
    }
  ~App(void){};
  int init(void);
  void process(void);

  private:
  Ticker ledToggle;
  DigitalIn modePin;
  DigitalOut led1;
  fileManager fileInst;
  int mode;
  event_callback_t ledToggleCB;
  flowmeter f;
  AnalogIn m_audio1;
  AnalogIn m_audio2;
  bool fileOpen;

  int testIndex;
  bool testRunning;
  manual m_mtest;
  /**
  Internal methods
   */
  int getMode(void);
  void toggleLeds(void);
  void datasetTestWrapper(void);
  void manualTestWrapper(void);
  /*!
    Error flags
   */
};


#endif