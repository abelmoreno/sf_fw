#ifndef TESTS_H
#define TESTS_H

#include "mbed.h"

class iTest: public Thread
{
public:
    virtual void start(void) = 0;
    virtual void run(void) = 0;
    virtual bool done(void) = 0;
    virtual string * getResults(void) = 0;

protected:
    
};

#endif