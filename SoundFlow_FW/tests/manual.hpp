#ifndef MANUAL_H
#define MANUAL_H

#include "mbed.h"
#include "iTest.hpp"
#include "filemanager.hpp"
#include "appConfig.hpp"

class manual: public iTest
{
public:
    manual(fileManager &file, AnalogIn &audioSignal1, AnalogIn &audioSignal2): 
        m_done(false),
        m_thread(osPriorityNormal),
        m_file(&file),
        m_audio1(&audioSignal1),
        m_audio2(&audioSignal2),
        m_testindex(0),
        m_format(TEST_FORMAT_MANUAL)
        {}

    void run(void);

    void start(void)
    {
        m_thread.start(callback(this, &manual::run));
        event_start.set(1);
        m_done = false;
        m_state = TEST_STATE_CREATE_FILE;
        m_testindex ++;
        acqCounter = 0;
    }

    bool done(void)
    { 
        return m_done;
    }

    string * getResults(void){return &result;}

    void convertQueueToLogString(sDataLog * mailValues, int size);

protected:
    bool m_done;
    string result;
    Thread m_thread;
    EventFlags event_start;
    fileManager * m_file;
    AnalogIn * m_audio1;
    AnalogIn * m_audio2;
    uint8_t m_testindex;
    uint32_t dataAcqArray[ACQ_DATA_ARRAY_SIZE];
    int acqCounter;
    eTestDataFormat m_format;
private:
  typedef enum
  {
    TEST_STATE_CREATE_FILE,
    TEST_STATE_ACQ,
    TEST_STATE_WAIT_STOP,
    TEST_STATE_CLOSE_FILE
  }eStates; 
  eStates m_state; 
};

#endif