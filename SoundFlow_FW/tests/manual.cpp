#include "manual.hpp"

void manual::run(void)
{
    bool testDone = false;
    int errLog;

while(true)
{
    switch(m_state)
    {
        case TEST_STATE_CREATE_FILE:
            event_start.wait_any(1);
            if(m_file->newTestFile(m_testindex,"Audio1\tAudio2\tFlow") == 0)
            {
                printf("Succesfuly created test file \n\r");
            }
            m_state = TEST_STATE_ACQ;
            acqCounter = 0;
            break;

        case TEST_STATE_ACQ:
            dataAcqArray[acqCounter] = (m_audio2->read_u16() << 16) | (0x0000ffff & m_audio1->read_u16());
            acqCounter++;
            if(acqCounter >= ACQ_DATA_ARRAY_SIZE)
            {
                acqCounter = 0;
                errLog = m_file->logValues(dataAcqArray, ACQ_DATA_ARRAY_SIZE, m_format);
                if(errLog == 1)
                {
                    wait_us(MAN_TEST_SAMPLE_PERIOD_US);
                }
                else 
                {
                    printf("Out of memory: <%d> errCode\n\r", errLog);
                    m_state = TEST_STATE_WAIT_STOP;
                }
            }
            else 
            {
                wait_us(MAN_TEST_SAMPLE_PERIOD_US);
            }            
            break;

        case TEST_STATE_WAIT_STOP:
            // Delay added to allow time to clean the filemanager queue
            if(m_file->queueEmpty() == true)
                m_state = TEST_STATE_CLOSE_FILE;
            ThisThread::sleep_for(1000);
            break;

        case TEST_STATE_CLOSE_FILE:
            printf("File closed, test done! \n\r");
            m_state = TEST_STATE_CREATE_FILE;
            m_done = true;
            break;
    }
}
}
