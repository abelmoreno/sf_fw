
#ifndef APPCONFIG_H
#define APPCONFIG_H

#define ENABLE_MANUAL_TEST  1
#define ENABLE_DATASET_TEST 0


#define FILEMANAGER_SD_FREQ             25000000
#define FILEMANAGER_DEBUG               1
#define FILEMANAGER_SERVER_MAX_LEN      256
#define FILEMANAGER_IP_MAX_LEN          16
#define FILEMANAGER_ID_MAX_LEN          16
#define FILEMANAGER_BOOL_MAX_LEN        5
#define FILEMANAGER_INT_MAX_LEN         6
#define FILEMANAGER_PACKET_MAX_TOKEN    200
#define FILEMANAGER_PACKET_MAX_LEN      32
#define MAN_TEST_SAMPLE_PERIOD_US       50

#if (ENABLE_MANUAL_TEST)
    #define ACQ_DATA_ARRAY_SIZE             50
    #define FILEMANAGER_QUEUE_SIZE          2000 
#else
    #define ACQ_DATA_ARRAY_SIZE    1
    #define FILEMANAGER_QUEUE_SIZE          8000
#endif
typedef enum 
{
    TEST_FORMAT_MANUAL,
    TEST_FORMAT_DATASET
}eTestDataFormat;

#endif