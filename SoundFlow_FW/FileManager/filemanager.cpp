/*TBD */
#include "filemanager.hpp"


SDBlockDevice sd(MBED_CONF_SD_SPI_MOSI, MBED_CONF_SD_SPI_MISO, MBED_CONF_SD_SPI_CLK, MBED_CONF_SD_SPI_CS);
FATFileSystem fs("fs");

void fileManager::run(void)
{
    while(1)
    {
        sDataLog *data = mail.try_get_for(Kernel::wait_for_u32_forever);
        if(data != nullptr)
        {
            this->convertQueueToLogString(data,ACQ_DATA_ARRAY_SIZE);
            this->_logString();
            //printf("%d Audio %d Flow \n\r", data->audio, data->flow);
            mail.free(data);
        }
        else 
        {
            printf("Error poping \n\r");
        }
    }
}

void fileManager::open(void)
{
    this->log.open(&fs, "log.txt", O_RDWR | O_APPEND);
}

void fileManager::openTestFile(int index)
{
    string name;
    name = "test_" + to_string(index) + ".txt";
    this->log.open(&fs, name.c_str(), O_RDWR | O_APPEND);
}

void fileManager::close(void)
{
    this->log.close();
}

void fileManager::start(void)
{
    thread_.start(callback(this, &fileManager::run));
}

int fileManager::init(void)
{
    int sdError = sd.init();
    if (sdError != 0)
        return -1;
    if (0 != sd.frequency(FILEMANAGER_SD_FREQ)) {
#if (FILEMANAGER_DEBUG)
        printf("Error setting frequency \n");
#endif
    }

    if (0 != sd.erase(0, sd.get_erase_size())) {
#if (FILEMANAGER_DEBUG)
        printf("Error Erasing block \n");
#endif
    }  
#if (FILEMANAGER_DEBUG)
        printf("sd size: %llu\n",         sd.size());
#endif
        if (0 != sd.erase(0, sd.get_erase_size())) {
            printf("Error Erasing block \n");
        }

        int err = fs.mount(&sd);
#if (FILEMANAGER_DEBUG)
        printf("%s\n", (err ? "Fail :(" : "OK"));
#endif
        if(err != 0)
            return -2;

    return 0;
}

int fileManager::logFileInit(void)
{
    int err = 0; 

    char openMessage[] = "Audio\tFlow\tBuffer\n";
    int size = strlen(openMessage);
    
    int errFile = this->log.open(&fs, "log.txt", O_RDWR | O_APPEND);
    if(errFile == 0)
    {
        this->log.write(openMessage, size);  
        this->log.close();
    }
    else 
    {
        err = -1;
#if (FILEMANAGER_DEBUG)
        printf("Unable to open log.txt file !: eCode <%d>  \n\r",errFile);
#endif
    }
    return err;
}

int fileManager::newTestFile(int index, string header)
{
    int err = 0;
    string name;

    name = "test_" + to_string(index) + ".txt";

    int errFile = this->log.open(&fs, name.c_str(), O_RDWR | O_APPEND);
    if(errFile == 0)
    {  this->string2Log = header;
        this->_logString();
    }
    else 
    {
        err = -1;
#if (FILEMANAGER_DEBUG)
        printf("Unable to open log.txt file !: eCode <%d>  \n\r",errFile);
#endif
    }
    
    return err;
}

bool fileManager::queueEmpty(void)
{
    bool empty;
    empty = mail.empty();
    if(empty)
        this->log.close();
    return empty;
}

int fileManager::logValues(uint32_t * dataPtr, int dataSize, eTestDataFormat format)
{
    int status = 1;
    if(mail.full() != true)
    {
        sDataLog * data = mail.try_alloc();
        if(data != nullptr)
        {
            memcpy(data->data1, dataPtr, sizeof(data) * dataSize);
            data->format = format;
            mail.put(data);
        }
        else 
        {
            status = -1;
        }
    }
    else
    {
        status = -2;
    }
    return status;
}

void fileManager::_logString(void)
{
    int bytesWritten;
    
    bytesWritten = this->log.write(string2Log.c_str(),string2Log.length());
    if(bytesWritten != string2Log.length())
    {
        printf("Error writting to file: bytes to write  \n\r");
    }
}

void fileManager::convertQueueToLogString(sDataLog * mailValues, int size)
{
    this->string2Log.clear();
    switch(mailValues->format)
    {
        case TEST_FORMAT_DATASET:
            for(int i = 0; i < size; i++)
            {
                if(i==0)
                {

                    this->string2Log = this->string2Log 
                    + to_string((unsigned int)mailValues->data1[i] & 0x0000ffff) 
                    + '\n' + to_string((unsigned int)(mailValues->data1[i] >> 16) & 0x0000ffff) 
                    + '\t' + to_string((unsigned int)(this->m_flow->getInstantFlow() * 1000)) 
                    + '\n';
                }
                else 
                {
                    this->string2Log = this->string2Log 
                    + to_string((unsigned int)mailValues->data1[i] & 0x0000ffff) 
                    + '\n' + to_string((unsigned int)(mailValues->data1[i] >> 16) & 0x0000ffff)
                    + '\n';
                }
            }
        break;

        case TEST_FORMAT_MANUAL:
            for(int i = 0; i < size; i++)
            {
                if(i==0)
                {
                    this->string2Log = this->string2Log 
                    + to_string((unsigned int)mailValues->data1[i] & 0x0000ffff) 
                    + '\t' + to_string((unsigned int)(mailValues->data1[i] >> 16) & 0x0000ffff) 
                    + '\t' + to_string((unsigned int)(this->m_flow->getInstantFlow() * 1000)) 
                    + '\n';
                }
                else 
                {
                    this->string2Log = this->string2Log 
                    + to_string(mailValues->data1[i] & 0x0000ffff) 
                    + '\t' + to_string((unsigned int)(mailValues->data1[i] >> 16) & 0x0000ffff)
                    + '\n';
                }
            }
        break;

        default:

        break;
    }

}

