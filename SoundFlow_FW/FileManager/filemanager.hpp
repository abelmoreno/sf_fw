
#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include "mbed.h"
#include "FATFileSystem.h"
#include "USBMSD.h"
#include "SDBlockDevice.h"
#include "platform/CircularBuffer.h"
#include "Json.h"
#include "config.h"
#include "appConfig.hpp"
#include <string>
#include "flowmeter.hpp"

typedef enum
{
    FILEMENAGER_QUEUE_EVENT_WRITE_LOG,
    FILEMANAGER_QUEUE_EVENT_WRITE_CONFIG,
    FILEMANAGER_QUEUE_EVENT_MAX
}eFileQueueEvent;

typedef struct
{
    int data1[ACQ_DATA_ARRAY_SIZE];
    eTestDataFormat format;
}sDataLog;

class fileManager
{
    public:
    fileManager(void):
    log(),
    thread_(osPriorityBelowNormal7)
     {};
    ~fileManager(){};
    /** init initializes sd card and mounts fat file system.

         @param none

         @return 0 on success, negative number otherwise
         */
    int init(void);
    /** logFileInit Tries to open log.txt file, if present returns, if not 
        present, creates a log.txt file with a welcome message.

         @param none

         @return 0 on success, negative number otherwise
         */
    int logFileInit(void);
    
    /** start

         @param none

         @return none
         */
    void start(void);
    /** process

         @param none

         @return none
         */
    void run(void);
    
    /** logValues Sets an event to write values on the file.

         @param int * audioSample, int * flowSample.

         @return none
         */
    int logValues(uint32_t * dataPtr, int dataSize, eTestDataFormat format);
    bool queueEmpty(void);
    void setFlowInstance(flowmeter  *flowInstance){m_flow = flowInstance;}
    void open(void);
    void close(void);

    int newTestFile(int index,string header);
    void openTestFile(int index);
    private:

    File log;
    Mail<sDataLog, FILEMANAGER_QUEUE_SIZE> mail;
    flowmeter * m_flow;
    string string2Log;
    Thread thread_;
    void _logString(void);  
    void convertQueueToLogString(sDataLog * mailValues, int size);
    /*!
        Filemanager log messages
    */
};

#endif