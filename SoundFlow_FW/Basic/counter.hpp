#ifndef COUNTER_H
#define COUNTER_H

#include "mbed.h"

class Counter {
public:
    Counter(PinName pin) : _interrupt(pin)          // create the InterruptIn on the pin specified to Counter
    {
        _interrupt.rise(callback(this, &Counter::increment)); // attach increment function of this counter instance
    }

    void increment()
    {
        _count++;
    }

    float read()
    {
        return _count;
    }
    void reset()
    {
        _count = 0;
    }
private:
    InterruptIn _interrupt;
    volatile float _count;
};

#endif