/*
    file: main.cpp
    M2M FW: Mqtt to modbus stick firmware

    Development Info:
    - Download and install Mbed Studio from: https://os.mbed.com/studio/
    - Mbed API information: https://os.mbed.com/docs/mbed-os/v5.15/apis/index.html
    - Hardware information: 
 */
#include "mbed.h"
#include "app.h"

/* Create application object */
App app;
/* ----------------------- Defines ------------------------------------------*/

/* ----------------------- Static variables ---------------------------------*/

int main()
{
    /**
        Initialize application
     */
    app.init();
    
    while(1)
    {    
        app.process();     
    }
    return 0;
}
