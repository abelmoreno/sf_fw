import matplotlib.pyplot as plt
import numpy as np
import csv
from scipy import fft, ifft
import numpy as np

T = 0.000050 #Sampling period(time between samples in Seconds)
DISCARD_INITIAL_SAMPLES_FROM_RAW_DATA  = 2000
NUMBER_OF_SAMPLES_FOR_REFERENCE_COMPUTATION = 2000

sound1RawData = []
sound2RawData = []

ref1 = 0
ref2 = 0

sound1 = []
sound2 = []

flow = []
x = []

def plot(dir,file):
    
    sound1.clear()
    sound2.clear()
    flow.clear()
    x.clear()
    sound1RawData.clear()
    sound2RawData.clear()
    getRawData(dir , file)
    getReference()

    plt.plot(x, sound1RawData, marker = 'o')
    plt.plot(x, sound2RawData, marker='o')
    #plt.plot(x, flow, marker = 'o')
    plt.savefig(file.replace('.txt','') +'_plot.pdf')
    plt.show()
        

def plotfft(file):
    
    i = len(x)
    i_ = i / 2
    int(i_)
    j = 0
    # Substract reference to raw sound signal
    while j < (i - 1):
        sound1.append(sound1RawData[j])
        sound2.append(sound2RawData[j])
        j = j + 1

    yf1 = fft(sound1)
    yf2 = fft(sound2)
    xf = np.linspace(0.0, 1.0/(2.0*T), i//2)

    plt.plot(xf, 2.0 / i * np.abs(yf1[0:i // 2]))
    plt.plot(xf, 2.0 / i * np.abs(yf2[0:i // 2]))
    plt.grid()
    plt.savefig(file.replace('.txt', '') + '_fft.pdf')
    plt.show()


#def plotRms(file):
    # https://blog.endaq.com/matlab-vs-python-speed-for-vibration-analysis-free-download must read before


def getRawData(dir , file):

    i = 0
    with open(dir + '/' + file, 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter='\t')

        while i < DISCARD_INITIAL_SAMPLES_FROM_RAW_DATA:
            next(plots)
            i = i + 1
        i = 0
        for row in plots:
            if i != 0:
                x.append(i)
                sound1RawData.append(int(row[0]))
                sound2RawData.append(int(row[1]))
            i += 1


def getReference():
    i = 0
    sum1 = 0
    sum2 = 0
    global ref1
    global ref2
    while i < NUMBER_OF_SAMPLES_FOR_REFERENCE_COMPUTATION:
        sum1 = sum1 + sound1RawData[i]
        sum2 = sum2 + sound2RawData[i]
        i = i+1

    ref1 = sum1 / NUMBER_OF_SAMPLES_FOR_REFERENCE_COMPUTATION
    ref2 = sum2 / NUMBER_OF_SAMPLES_FOR_REFERENCE_COMPUTATION

