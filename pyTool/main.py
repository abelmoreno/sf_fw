import os
from helpers import plotfft,plot
import sys

dir_path = "G:" # Set path to raw data

for root, dirs, files in os.walk(dir_path):
    for file in files:
        if(file.endswith('.txt')):
            plot(dir_path,file)
            plotfft(file)