import matplotlib.pyplot as plt
import numpy as np
import csv
from scipy import fft, ifft
import numpy as np

T = 0.000050 #Sampling period(time between samples in Seconds)

sound = []
flow = []
x = []

def plot(dir,file):
    
    sound.clear()
    flow.clear()
    x.clear()
    i = 0
    with open(dir + '/' + file, 'r') as csvfile:
        plots= csv.reader(csvfile, delimiter='\t')
        for row in plots:
            if (i != 0):
                x.append(i)
                sound.append(int(row[0]))
                flow.append(int(row[1]))
            i += 1
        plt.plot(x, sound, marker = 'o')       
        plt.plot(x, flow, marker = 'o')
    plt.savefig(file.replace('.txt','') +'_plot.pdf')
    plt.show()
        

def plotfft(file):
    
    i = len(x)
    yf = fft(sound)
    xf = np.linspace(0.0, 1.0/(2.0*T), i//2)

    plt.plot(xf, 2.0/i * np.abs(yf[0:i//2]))
    plt.grid()
    plt.savefig(file.replace('.txt','') + '_fft.pdf')
    plt.show()

#def plotRms(file):
    # https://blog.endaq.com/matlab-vs-python-speed-for-vibration-analysis-free-download must read before